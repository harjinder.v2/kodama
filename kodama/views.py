
from flask import request, Blueprint, render_template, url_for, redirect
from flask_login import current_user, login_user, login_required, logout_user
from werkzeug.urls import url_parse

from kodama.forms import HotelRegistrationForm, LoginForm, UserRegistrationForm
from kodama.forms import HotelReportForm
from kodama.models import User, Hotel, HotelReport
from kodama import app, db
from kodama.core.report import submit_report

# mod = Blueprint('module', __name__, template_folder='templates', static_folder='static')

# --------------------------------------------------------------------------------------------------
# Home Page
# --------------------------------------------------------------------------------------------------


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')


# --------------------------------------------------------------------------------------------------
# User Login Page
# --------------------------------------------------------------------------------------------------


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None or not user.check_password(form.password.data):
            msg = 'Invalid username or password'
            return render_template('message.html', message=msg)

        login_user(user)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)

    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register_user', methods=['GET', 'POST'])
def register_user():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = UserRegistrationForm()
    if form.validate_on_submit():
        user = User(email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        msg = 'Congratulations, you are now a registered user!'
        return render_template('message.html', message=msg)
    return render_template('register_user.html', title='Register User', form=form)


# --------------------------------------------------------------------------------------------------
# Hotel Registration Page
# --------------------------------------------------------------------------------------------------


@app.route('/register_hotel', methods=['GET', 'POST'])
def register_hotel():
    # TODO: check if current user is already associated with a hotel
    form = HotelRegistrationForm()
    if form.validate_on_submit():
        hotel = Hotel(name=form.name.data,
                      area=form.area.data,
                      address=form.address.data,
                      city=form.city.data,
                      state=form.state.data,
                      country=form.country.data,
                      zip=form.zip.data)
        db.session.add(hotel)
        db.session.commit()

        print("Hotel: {0}".format(current_user.hotel_id))
        user = User.query.filter_by(id=current_user.id).first()
        user.set_hotel(hotel)
        db.session.commit()
        msg = 'Congratulations, your hotel is now registered!'
        return render_template('message.html', message=msg)
    return render_template('register_hotel.html', title='Register Hotel', form=form)


@app.route('/report', methods=['GET', 'POST'])
def report():
    form = HotelReportForm()
    if form.validate_on_submit():
        hotel = Hotel.query.filter_by(id=current_user.hotel_id).first()
        hotel_report = HotelReport(hotel,
                                   form.from_date.data,
                                   form.to_date.data,
                                   form.electricity.data,
                                   form.water.data,
                                   form.waste.data)
        submit_report(hotel_report)
        msg = 'Report submitted successfully!'
        return render_template('message.html', message=msg)

    return render_template('report.html', title='Report', form=form)

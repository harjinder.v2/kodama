import os
import logging
from logging.handlers import RotatingFileHandler

from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager


app = Flask(__name__)

# load the instance config
app.config.from_object('kodama.default_config')
app.config.from_pyfile(os.path.join(app.instance_path, 'config.py'))

# setup database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db = SQLAlchemy(app)

# encryption for passwords
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'

# setup logging
# Reference: https://stackoverflow.com/questions/14037975/how-do-i-write-flasks-excellent-debug-log-message-to-a-file-in-production
log_filename = app.config['LOG_FILENAME']
log_level = app.config['LOG_LEVEL']
file_handler = RotatingFileHandler(log_filename,
                                   maxBytes=1024 * 1024 * 100,
                                   backupCount=20)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
file_handler.setFormatter(formatter)
app.logger.addHandler(file_handler)
app.logger.setLevel(log_level)
app.logger.info('Added file logger successfully. Logs will be in {0}'.format(log_filename))

# @app.errorhandler(404)
# def page_not_found(exception):
#     app.logger.exception(exception)
#     return render_template('404.html')


@app.errorhandler(500)
def internal_error(exception):
    app.logger.exception(exception)
    return render_template('500.html')


import kodama.views

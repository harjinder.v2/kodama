from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, FloatField, IntegerField
from wtforms.validators import DataRequired, Email, ValidationError, EqualTo
from kodama.models import User


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired("Missing"), Email("InvalidEmail")])
    password = PasswordField('Password', validators=[DataRequired("Missing")])


class UserRegistrationForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')


class HotelRegistrationForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired("Missing")])
    area = IntegerField('Area (square feet)', validators=[DataRequired("Missing")])
    address = StringField('Address', validators=[DataRequired("Missing")])
    city = StringField('City', validators=[DataRequired("Missing")])
    state = StringField('State', validators=[DataRequired("Missing")])
    country = StringField('Country', validators=[DataRequired("Missing")])
    zip = StringField('Zip Code', validators=[DataRequired("Missing")])


class HotelReportForm(FlaskForm):
    from_date = StringField('From', validators=[DataRequired("Missing")])
    to_date = StringField('To', validators=[DataRequired("Missing")])
    electricity = FloatField('Electricity (kWh)', validators=[DataRequired("Missing")])
    water = FloatField('Water (cubic meter)', validators=[DataRequired("Missing")])
    waste = FloatField('Waste (kg)', validators=[DataRequired("Missing")])

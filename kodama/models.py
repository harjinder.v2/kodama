
from flask_login import UserMixin
from kodama import bcrypt, db, login_manager


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    hotel_id = db.Column(db.Integer, db.ForeignKey('hotel.id'), nullable=True)
    hotel = db.relationship('Hotel', backref=db.backref('users', lazy=True))
    email = db.Column(db.String(120), unique=True, nullable=False)
    password_hash = db.Column(db.String(120), nullable=False)

    def __repr__(self):
        return '<User %r>' % self.username

    def set_password(self, password):
        self.password_hash = bcrypt.generate_password_hash(password)

    def check_password(self, password):
        return bcrypt.check_password_hash(self.password_hash, password)

    def set_hotel(self, hotel):
        self.hotel_id = hotel.id


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


class Hotel(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(80), nullable=False)
    area = db.Column(db.Integer, nullable=False)
    address = db.Column(db.String(120), nullable=False)
    city = db.Column(db.String(120), nullable=False)
    state = db.Column(db.String(120), nullable=False)
    country = db.Column(db.String(120), nullable=False)
    zip = db.Column(db.String(120), nullable=False)

    def __repr__(self):
        return '<Hotel %r>' % self.name


class HotelReport():
    def __init__(self, hotel, from_date, to_date, electricity, water, waste):
        self.from_date = from_date
        self.to_date = to_date
        self.hotel = hotel
        self.electricity = electricity
        self.water = water
        self.waste = waste

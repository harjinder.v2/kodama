from kafka import KafkaProducer
import json
from datetime import datetime
import time

producer = None


def submit_report(hotel_report):
    global producer
    if producer is None:
        producer = KafkaProducer(bootstrap_servers='localhost:9092')
                                 # value_serializer=lambda v: json.dumps(v).encode('utf-8'))

    print("Hotel Report: {0}, {1}, {2}, {3}, {4}, {5}".format(hotel_report.from_date,
                                                              hotel_report.to_date,
                                                              hotel_report.hotel,
                                                              hotel_report.electricity,
                                                              hotel_report.water,
                                                              hotel_report.waste))

    report = dict()
    report['id'] = hotel_report.hotel.id
    report['name'] = hotel_report.hotel.name
    report['area'] = hotel_report.hotel.area
    report['city'] = hotel_report.hotel.city
    report['state'] = hotel_report.hotel.state
    report['country'] = hotel_report.hotel.country
    report['zip'] = hotel_report.hotel.zip

    report['electricity'] = hotel_report.electricity
    report['water'] = hotel_report.water
    report['waste'] = hotel_report.waste

    report['consumption_date'] = hotel_report.from_date
    report['timestamp'] = datetime.utcfromtimestamp(time.time()).isoformat() + ".000Z"

    print(json.dumps(report))
    producer.send(topic='hotel_daily_consumption', value=json.dumps(report).encode('utf-8'))
    producer.flush()
